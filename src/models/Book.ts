export class Book {
  titre: string;
  auteur: string;
  genre: string;
  description: string[];
  prix: number;
  isLend: boolean;
  userName:string;

  constructor(titre: string, auteur: string, genre: string, description: string[], prix: number, isLend: boolean,userName:string) {
    this.titre = titre;
    this.auteur = auteur;
    this.genre = genre;
    this.description = description;
    this.prix = prix;
    this.isLend = isLend;
    this.userName = userName
  }
}
