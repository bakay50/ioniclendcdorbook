export class Cd {
  titre: string;
  auteur: string;
  description: string[];
  prix: number;
  isLend: boolean;
  userName:string;

  constructor(titre: string, auteur: string, description: string[], prix: number, isLend: boolean,userName:string) {
    this.titre = titre;
    this.auteur = auteur;
    this.description = description;
    this.prix = prix;
    this.isLend = isLend;
    this.userName = userName
  }
}
