import {Injectable} from "@angular/core";
import {LoadingController, ToastController} from "ionic-angular";


@Injectable()
export class UtilsService {

  constructor(private toastCtrl: ToastController,private loadingCtrl: LoadingController) {}

  displayLoading(message){
    let loader = this.loadingCtrl.create({
      content: message
    });
    return loader;
  }

  displayToast(message,duration,position){
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    })
    return toast;
  }


}
