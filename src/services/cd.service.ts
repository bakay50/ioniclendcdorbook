import {Cd} from "../models/Cd";
import {Subject} from "rxjs/Subject";
import * as firebase from "firebase";
import DataSnapshot = firebase.database.DataSnapshot;
import {Injectable} from "@angular/core";
import { Storage } from '@ionic/storage';
import * as constants  from "../constants/constants";

@Injectable()
export class CdService {

  cdsList$ = new Subject<Cd[]>();

  cdsList: Cd[] = [
    {
      titre: 'Love Me Do',
      auteur:'The Beatles',
      description: [
        'description:The Beatles est un groupe musical britannique, originaire de Liverpool, en Angleterre. Formé en 1960, ' +
        'et composé de John Lennon, Paul McCartney, George Harrison et Ringo Starr, il est considéré comme le groupe le plus populaire et influent de l\'histoire du rock.'+
        'En dix ans d\'existence et seulement huit ans de carrière discographique (de 1962 à 1970)'
      ],
      prix:2800,
      isLend: false,
      userName:''
    },
    {
      titre: 'Loving You',
      auteur:'Elvis Presley',
      description: [
        'description :Elvis Aaron Presley est né le 8 janvier 1935 à Tupelo, dans l\'État du Mississippi'+
        'dans une petite maison de type shotgun house construite par son père,'+
        'Vernon Elvis Presley (1916-1979), en prévision de sa naissance'
      ],
      prix:2300,
      isLend: false,
      userName:''
    },
    {
      titre: 'Ropin\' the Wind',
      auteur:'Garth Brooks',
      description: [
        'description : Garth Brooks connait un succès commercial phénoménal depuis les années 1990 avec plus de 70 chansons à succès lui permettant de vendre plus de 130 millions ' +
        'd\'albums aux États-Unis, ce qui fait de lui l\'un des meilleurs vendeurs de disques de tous les temps aux États-Unis'
      ],
      prix:3500,
      isLend: true,
      userName:'Marina'
    },
    {
      titre: 'Custard Pie',
      auteur:'Led Zeppelin',
      description: [
        'description : Led Zeppelin est un groupe de rock britannique, originaire de Londres, en Angleterre. Il est fondé en 1968 par Jimmy Page (guitare), ' +
        'avec Robert Plant (chant), John Paul Jones (basse, claviers) et John Bonham (batterie), ' +
        'et dissous à la suite de la mort de ce dernier'
      ],
      prix:3700,
      isLend: true,
      userName:'Julien'
    }
  ];

  constructor(private storage: Storage) {
    this.saveCdList();
    this.fetchCdList();
  }


  onToggleCd(cd :Cd){
    cd.isLend = !cd.isLend;
  }

  saveCdList() {
    this.storage.set('cdviews',this.cdsList);
  }

  fetchCdList() {
    this.storage.get('cdviews').then(
      (list) => {
        if (list && list.length) {
          this.cdsList = list.slice();
        }
        this.emitCds();
      }
    );
  }

  emitCds() {
    this.cdsList$.next(this.cdsList.slice());
  }


  saveCdData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('cds').set(this.cdsList).then(
        (data: DataSnapshot) => {
          resolve(data);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  retrieveCdData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('cds').once('value').then(
        (data: DataSnapshot) => {
          this.cdsList = data.val();
          this.saveCdList();
          this.emitCds();
          resolve(constants.SUCCES_DATA);
        }, (error) => {
          reject(error);
        }
      );
    });
  }



}
