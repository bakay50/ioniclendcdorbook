import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuController, ModalController} from 'ionic-angular';
import {LendCdPage} from "../lend-cd/lend-cd";
import {Cd} from "../../models/Cd";
import {CdService} from "../../services/cd.service";
import {Subscription} from "rxjs/Subscription";


@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage implements OnInit, OnDestroy{

  cdsList: Cd[];
  cdSubscription: Subscription;

  constructor(private menuCtrl: MenuController, public cdService: CdService, private modalCtrl: ModalController) {}

  ngOnInit() {
    this.cdSubscription = this.cdService.cdsList$.subscribe(
      (cds: Cd[]) => {
        this.cdsList = cds.slice();
      }
    );
    this.cdService.fetchCdList();
  }

  ngOnDestroy() {
    this.cdSubscription.unsubscribe();
  }


  onLoadCd(index: number) {
    let modal = this.modalCtrl.create(LendCdPage, {index: index});
    modal.present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }



}
