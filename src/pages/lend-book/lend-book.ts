import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Book} from "../../models/Book";
import {BooksService} from "../../services/books.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";



@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html',
})
export class LendBookPage {

  index: number;
  book: Book;
  empruntBookForm: FormGroup;
  userName :String;

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController,
              public booksService: BooksService,
              private formBuilder : FormBuilder) {}

  initForm() {
    this.empruntBookForm = this.formBuilder.group({
      userName: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.initForm();
    this.index = this.navParams.get('index');
    this.book = this.booksService.booksList[this.index];
    this.userName = this.book.userName;
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }


  onToggleBook() {
    this.booksService.onToggleBook(this.book);
  }

  onSubmitFormBook() {
    this.userName = this.empruntBookForm.get('userName').value;
    this.book.userName = this.userName;
    this.booksService.booksList[this.index].userName = this.book.userName;
    this.booksService.saveBookList();
    this.dismissModal();
  }


  onDeleteBookEmprunt() {
    this.userName = '';
    this.book.userName = '';
    this.booksService.booksList[this.index].userName = this.book.userName;
    this.booksService.saveBookList();
    this.dismissModal();
  }


}
