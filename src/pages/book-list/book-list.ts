import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuController, ModalController} from 'ionic-angular';
import {Book} from "../../models/Book";
import {BooksService} from "../../services/books.service";
import {LendBookPage} from "../lend-book/lend-book";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})
export class BookListPage implements OnInit, OnDestroy{

  booksList: Book[];
  bookSubscription: Subscription;

  constructor(private menuCtrl: MenuController, public booksService: BooksService, private modalCtrl: ModalController) {}

  ngOnInit() {
    this.bookSubscription = this.booksService.bookList$.subscribe(
      (books: Book[]) => {
        this.booksList = books.slice();
      }
    );
    this.booksService.fetchBookList();
  }

  ngOnDestroy() {
    this.bookSubscription.unsubscribe();
  }

  onLoadBook(index: number) {
    let modal = this.modalCtrl.create(LendBookPage, {index: index});
    modal.present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }



}
