import {Component} from '@angular/core';
import {MenuController} from 'ionic-angular';
import {BooksService} from "../../services/books.service";
import {CdService} from "../../services/cd.service";
import {UtilsService} from "../../services/utils.service";
import * as constants  from "../../constants/constants";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(private menuCtrl: MenuController,
              private booksService:BooksService,
              private cdService : CdService,
              private utilsService : UtilsService) {}

  onToggleMenu() {
    this.menuCtrl.open();
  }


  saveBookOnBackend(){
    let loader = this.utilsService.displayLoading(constants.SAUVEGARDE_MESSAGE);
    loader.present();
    this.booksService.saveBookData().then(
      () => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(constants.SUCCES_SAUVEGARDE_MESSAGE,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      },
      (error) => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(error,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      }
    );
  }

  retrieveBookFromBackend(){
    let loader = this.utilsService.displayLoading(constants.RECUPERATION_MESSAGE);
    loader.present();
    this.booksService.retrieveBookData().then(
      () => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(constants.SUCCES_RECUPERATION_MESSAGE,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      },
      (error) => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(error,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      }
    );
  }

  saveCdOnBackend(){
    let loader = this.utilsService.displayLoading(constants.SAUVEGARDE_MESSAGE);
    loader.present();
    this.cdService.saveCdData().then(
      () => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(constants.SUCCES_SAUVEGARDE_MESSAGE,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      },
      (error) => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(error,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      }
    );
  }

  retrieveCdFromBackend(){
    let loader = this.utilsService.displayLoading(constants.RECUPERATION_MESSAGE);
    loader.present();
    this.cdService.retrieveCdData().then(
      () => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(constants.SUCCES_RECUPERATION_MESSAGE,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      },
      (error) => {
        loader.dismiss();
        let toastCtrl = this.utilsService.displayToast(error,constants.DURATION,constants.POSITION);
        toastCtrl.present();
      }
    );
  }


}
