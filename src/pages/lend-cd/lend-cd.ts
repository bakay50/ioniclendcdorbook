import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Cd} from "../../models/Cd";
import {CdService} from "../../services/cd.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage {

  index: number;
  cd: Cd;
  empruntCdForm: FormGroup;
  userName :String;

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController,
              public cdService: CdService,
              private formBuilder : FormBuilder) {}

  initForm() {
    this.empruntCdForm = this.formBuilder.group({
      userName: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.initForm();
    this.index = this.navParams.get('index');
    this.cd = this.cdService.cdsList[this.index];
    this.userName = this.cd.userName;
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  onToggleCd() {
    this.cdService.onToggleCd(this.cd);
  }

  onSubmitFormCd() {
    this.userName = this.empruntCdForm.get('userName').value;
    this.cd.userName = this.userName;
    this.cdService.cdsList[this.index].userName = this.cd.userName;
    this.cdService.saveCdList();
    this.dismissModal();
  }


  onDeleteCdEmprunt() {
    this.userName = '';
    this.cd.userName = '';
    this.cdService.cdsList[this.index].userName = this.cd.userName;
    this.cdService.saveCdList();
    this.dismissModal();
  }

}
