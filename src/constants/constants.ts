export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyABW1iXbjZTFrFO9pZFFiKSCAafmXbOz84",
  authDomain: "openclassionic.firebaseapp.com",
  databaseURL: "https://openclassionic.firebaseio.com",
  projectId: "openclassionic",
  storageBucket: "openclassionic.appspot.com",
  messagingSenderId: "202519020729"
};
export const SAUVEGARDE_MESSAGE = 'Sauvegarde en cours…';
export const SUCCES_SAUVEGARDE_MESSAGE = 'Données sauvegardées !';
export const RECUPERATION_MESSAGE = 'Récuperation en cours…';
export const SUCCES_RECUPERATION_MESSAGE = 'Données récupérées !';
export const DURATION = 3000;
export const POSITION = 'bottom';
export const SUCCES_DATA  = 'Données récupérées avec succès !';
